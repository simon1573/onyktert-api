package se.tuxflux.onyktert.model

import org.springframework.data.annotation.Id
import org.springframework.data.annotation.TypeAlias

@TypeAlias("adConfig")
data class AdConfig(@Id val id: String, val interstitialAdFrequency: Int, val showInterstitialAd: Boolean, val showBannerAd: Boolean)