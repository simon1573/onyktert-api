package se.tuxflux.onyktert.repository

import org.springframework.data.repository.CrudRepository
import se.tuxflux.onyktert.model.StatementType
import se.tuxflux.onyktert.model.SubmittedStatement


interface SubmittedStatementRepository : CrudRepository<SubmittedStatement, Long> {
    fun findByType(type: StatementType): List<SubmittedStatement>
    fun findByText(text: String): List<SubmittedStatement>
}