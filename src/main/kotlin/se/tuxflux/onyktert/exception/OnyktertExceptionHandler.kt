package se.tuxflux.onyktert.exception

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestControllerAdvice

@RestControllerAdvice
class OnyktertExceptionHandler {
    @RequestMapping(produces = ["application/json"])
    @ExceptionHandler(value = [(AlreadyExistsException::class)])
    @ResponseStatus(value = HttpStatus.CONFLICT)
    fun handleAlreadyExistsException(ex: AlreadyExistsException) {
    }

    @RequestMapping(produces = ["application/json"])
    @ExceptionHandler(value = [(DoesNotExistsException::class)])
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    fun handleDoesNotExistsException(ex: DoesNotExistsException) {
    }

    @RequestMapping(produces = ["application/json"])
    @ExceptionHandler(value = [(ForbiddenException::class)])
    @ResponseStatus(value = HttpStatus.FORBIDDEN)
    fun handleForbiddenException(ex: ForbiddenException) {
    }

    @RequestMapping(produces = ["application/json"])
    @ExceptionHandler(value = [(MissingFieldException::class)])
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    fun handleMissingFieldException(ex: MissingFieldException) {
    }
}