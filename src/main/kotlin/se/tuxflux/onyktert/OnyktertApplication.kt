package se.tuxflux.onyktert

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import java.io.File
import java.lang.IllegalStateException
import java.util.*

@SpringBootApplication
class OnyktertApplication
private val logger: Logger = LoggerFactory.getLogger("OnyktertApplication")

fun main(args: Array<String>) {
    createApiKeyIfNotExists()
    validateApiKeyExists()
    runApplication<OnyktertApplication>(*args)
}

fun createApiKeyIfNotExists() {
    val keyFile = File("api.key")
    logger.info("Checking if ${keyFile.absolutePath} exists...")
    if (!keyFile.exists()) {
        logger.info("Created ${keyFile.absolutePath}!")
        keyFile.printWriter().use { out -> out.println(UUID.randomUUID().toString()) }
    } else {
        logger.info("${keyFile.absolutePath} exists already exists")
    }
}

fun validateApiKeyExists() {
    val apiKey = File("api.key").readText(Charsets.UTF_8)
    if (apiKey.isBlank()) {
        throw IllegalStateException("Api key should never be null")
    }
}
