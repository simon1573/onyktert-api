package se.tuxflux.onyktert.service

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import se.tuxflux.onyktert.exception.ForbiddenException
import java.io.File

interface AuthService {
    fun isValidApiKey(givenKey: String, throwOnError: Boolean = false): Boolean
}

@Service
class AuthServiceImpl : AuthService {
    private final val logger: Logger = LoggerFactory.getLogger("AuthService")
    private final val correctKey: String = File("api.key").readText(Charsets.UTF_8).trim()

    override fun isValidApiKey(givenKey: String, throwOnError: Boolean): Boolean {
        if (correctKey.isEmpty()) throw IllegalStateException("Api key should never be null")
        val isCorrect = correctKey == givenKey.trim()
        if (!isCorrect) {
            logger.info("Got invalid api key: $givenKey")
            if (throwOnError) {
                throw ForbiddenException("Invalid API key")
            }
        }
        return isCorrect
    }
}