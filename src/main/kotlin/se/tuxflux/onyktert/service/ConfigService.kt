package se.tuxflux.onyktert.service

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import se.tuxflux.onyktert.model.AdConfig
import se.tuxflux.onyktert.repository.AdConfigRepository
import java.util.*
import java.util.function.Consumer

interface ConfigService {
    fun getAdConfig(): AdConfig
    fun setAdConfig(adConfig: AdConfig): AdConfig
}

@Service
class ConfigServiceImpl : ConfigService {
    private final val logger: Logger = LoggerFactory.getLogger("ConfigService")

    @Autowired
    lateinit var adConfigRepository: AdConfigRepository

    override fun getAdConfig(): AdConfig {
        val adConfigs = adConfigRepository.findAll()
        if (adConfigs.none()) { // Lazy solution for populating an initial value
            val initialConfig = AdConfig(UUID.randomUUID().toString(), 50, true, true)
            adConfigRepository.save(initialConfig)
        }

        val config = adConfigs.first()
        logger.info("Fetching ad config: $config")
        return config
    }

    override fun setAdConfig(adConfig: AdConfig): AdConfig {
        logger.info("Replacing ad config. New config: $adConfig")
        adConfigRepository.findAll().forEach(Consumer { config -> adConfigRepository.delete(config) })
        adConfigRepository.save(adConfig)
        return adConfig
    }
}