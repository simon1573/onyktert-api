package se.tuxflux.onyktert.service

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import se.tuxflux.onyktert.exception.AlreadyExistsException
import se.tuxflux.onyktert.exception.DoesNotExistsException
import se.tuxflux.onyktert.model.StatementType
import se.tuxflux.onyktert.model.SubmittedStatement
import se.tuxflux.onyktert.model.SubmittedStatements
import se.tuxflux.onyktert.repository.SubmittedStatementRepository
import se.tuxflux.onyktert.service.helper.ValidationService
import java.security.SecureRandom

interface SubmittedStatementService {
    fun getNeverHaveIEver(includeSfw: Boolean, includeNsfw: Boolean): SubmittedStatements
    fun getPointingGame(includeSfw: Boolean, includeNsfw: Boolean): SubmittedStatements
    fun getInOtherWords(includeSfw: Boolean, includeNsfw: Boolean): SubmittedStatements
    fun getTruthOrDareTruths(includeSfw: Boolean, includeNsfw: Boolean): SubmittedStatements
    fun getTruthOrDareDares(includeSfw: Boolean, includeNsfw: Boolean): SubmittedStatements
    fun getTruthOrDareMixed(includeSfw: Boolean, includeNsfw: Boolean): SubmittedStatements

    fun addStatement(statement: SubmittedStatement)
    fun deleteStatement(id: Long)
}

@Service
class SubmittedStatementServiceImpl : SubmittedStatementService {
    private final val logger: Logger = LoggerFactory.getLogger("SubmittedStatementService")

    @Autowired
    lateinit var repository: SubmittedStatementRepository

    @Autowired
    lateinit var validationService: ValidationService

    override fun addStatement(statement: SubmittedStatement) {
        if (!repository.findByText(statement.text).isEmpty()) {
            throw AlreadyExistsException("'${statement.text}' already exist")
        }
        statement.id = validateId(statement.id)

        validationService.validateStatement(statement)
        logger.info("Persisting submitted statement: $statement")
        repository.save(statement)
    }

    override fun deleteStatement(id: Long) {
        val statement = repository.findById(id)
        if (!statement.isPresent) {
            throw DoesNotExistsException("'$id' does not exist")
        }
        logger.info("Removing submitted statement: $statement")
        repository.deleteById(id)
    }

    override fun getNeverHaveIEver(includeSfw: Boolean, includeNsfw: Boolean): SubmittedStatements {
        logger.info("Fetching all submitted StatementType.NEVER_HAVE_I_EVER")
        val result: List<SubmittedStatement> = repository.findByType(StatementType.NEVER_HAVE_I_EVER)
        return SubmittedStatements(result)
    }

    override fun getPointingGame(includeSfw: Boolean, includeNsfw: Boolean): SubmittedStatements {
        logger.info("Fetching all submitted StatementType.POINTING_GAME")
        val result: List<SubmittedStatement> = repository.findByType(StatementType.POINTING_GAME)
        return SubmittedStatements(result)
    }

    override fun getInOtherWords(includeSfw: Boolean, includeNsfw: Boolean): SubmittedStatements {
        logger.info("Fetching all submitted StatementType.IN_OTHER_WORDS")
        val result: List<SubmittedStatement> = repository.findByType(StatementType.IN_OTHER_WORDS)
        return SubmittedStatements(result)
    }

    override fun getTruthOrDareTruths(includeSfw: Boolean, includeNsfw: Boolean): SubmittedStatements {
        logger.info("Fetching all submitted StatementType.TRUTH_OR_DARE_TRUTH")
        val result: List<SubmittedStatement> = repository.findByType(StatementType.TRUTH_OR_DARE_TRUTH)
        return SubmittedStatements(result)
    }

    override fun getTruthOrDareDares(includeSfw: Boolean, includeNsfw: Boolean): SubmittedStatements {
        logger.info("Fetching all submitted StatementType.TRUTH_OR_DARE_DARE")
        val result: List<SubmittedStatement> = repository.findByType(StatementType.TRUTH_OR_DARE_DARE)
        return SubmittedStatements(result)
    }

    override fun getTruthOrDareMixed(includeSfw: Boolean, includeNsfw: Boolean): SubmittedStatements {
        logger.info("Fetching all submitted truth or dare statements")
        val result: List<SubmittedStatement> = repository.findByType(StatementType.TRUTH_OR_DARE)
        return SubmittedStatements(result)
    }

    private fun validateId(id: Long): Long {
        val statement = repository.findById(id)
        return if (statement.isPresent) {
            val newId = SecureRandom().nextLong()
            validateId(newId)
        } else {
            id
        }
    }
}