package se.tuxflux.onyktert.service.helper

import org.springframework.stereotype.Service
import se.tuxflux.onyktert.model.Statement
import java.util.*

@Service
class SanitationService {

    fun sanitizeStatement(dirtyStatement: Statement): Statement {
        val sanitizedText = dirtyStatement.text.trim()
        val now = Date()
        return Statement(id = dirtyStatement.id, text = sanitizedText, isNsfw = dirtyStatement.isNsfw, type = dirtyStatement.type, createdDate = now)
    }
}
