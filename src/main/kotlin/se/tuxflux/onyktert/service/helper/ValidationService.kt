package se.tuxflux.onyktert.service.helper

import org.springframework.stereotype.Service
import org.springframework.util.ObjectUtils.isEmpty
import se.tuxflux.onyktert.exception.MissingFieldException
import se.tuxflux.onyktert.model.Statement
import se.tuxflux.onyktert.model.SubmittedStatement

@Service
class ValidationService {

    fun validateStatement(statement: Statement) {
        if (isEmpty(statement.id)) throw MissingFieldException("id")
        if (isEmpty(statement.isNsfw)) throw MissingFieldException("isNsfw")
        if (isEmpty(statement.text)) throw MissingFieldException("text")
        if (isEmpty(statement.type)) throw MissingFieldException("type")
        if (isEmpty(statement.createdDate)) throw MissingFieldException("createdDate")
    }

    fun validateStatement(statement: SubmittedStatement) {
        if (isEmpty(statement.id)) throw MissingFieldException("id")
        if (isEmpty(statement.text)) throw MissingFieldException("text")
        if (isEmpty(statement.type)) throw MissingFieldException("type")
        if (isEmpty(statement.createdDate)) throw MissingFieldException("createdDate")
    }
}
